package ru.dolbak.myapplication;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.View;

public class MyDraw extends View {

    public MyDraw(Context context) {
        super(context);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        Paint paint = new Paint();
        paint.setColor(Color.WHITE);
        int y = 0;
        while (y < canvas.getHeight()){
            canvas.drawLine(0, y, canvas.getWidth(),y,paint);
            y+=100;
        }

    }
}
